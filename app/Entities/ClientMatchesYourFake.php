<?php


namespace App\Entities;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Contracts\Support\Jsonable;

/**
 * @Entity
 * @InheritanceType("JOINED")
 * @DiscriminatorColumn(name="client_discr", type="string")
 * @ORM\Table(name="client_matches_your_fake")
 */
class ClientMatchesYourFake extends ClientMatch
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(name="client_match_id", type="integer")
     */
    protected $clientMatchId;

    /**
     * ClientMatch constructor.
     * @param $clientMatchId
     */
    public function __construct(
        $clientMatchId
    ) {
        $this->setServiceClientMatchId($clientMatchId);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getServiceClientMatchId()
    {
        return $this->clientMatchId;
    }

    /**
     * @param $clientMatchId
     */
    public function setServiceClientMatchId($clientMatchId)
    {
        $this->clientMatchId = $clientMatchId;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            'id' => $this->getId(),
            'service_match_id' => $this->getServiceClientMatchId()
        ];
    }
}

