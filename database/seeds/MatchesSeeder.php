<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class MatchesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        $matches = [
            [
                'winner_id'  => 1,
                'start'      => Carbon::now(),
                'status'     => 1,
                'created_at' => Carbon::now()
            ],
            [
                'winner_id'  => 2,
                'start'      => Carbon::now(),
                'status'     => 1,
                'created_at' => Carbon::now()
            ],
            [
                'winner_id'  => 3,
                'start'      => Carbon::now(),
                'status'     => 1,
                'created_at' => Carbon::now()
            ],
            [
                'winner_id'  => 4,
                'start'      => Carbon::now(),
                'status'     => 1,
                'created_at' => Carbon::now()
            ],
            [
                'winner_id'  => 5,
                'start'      => Carbon::now(),
                'status'     => 1,
                'created_at' => Carbon::now()
            ],
        ];

        DB::table('matches')->truncate();
        DB::table('matches')->insert($matches);
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
