<?php

use Illuminate\Database\Seeder;
use App\Entities\Roster;
use Carbon\Carbon;

class RostersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        $rosters = [
            [
                'team_name' => 'test1',
                'created_at'  => Carbon::now(),
            ],
            [
                'team_name' => 'test2',
                'created_at'  => Carbon::now(),
            ],
            [
                'team_name' => 'test3',
                'created_at'  => Carbon::now(),
            ],
            [
                'team_name' => 'test4',
                'created_at'  => Carbon::now(),
            ],
            [
                'team_name' => 'test5',
                'created_at'  => Carbon::now(),
            ],
        ];

        DB::table('rosters')->truncate();
        DB::table('rosters')->insert($rosters);
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
