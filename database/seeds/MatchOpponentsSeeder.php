<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class MatchOpponentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        $matches = [
            [
                'roster_id'  => 1,
                'match_id'   => 1
            ],
            [
                'roster_id'  => 2,
                'match_id'   => 2
            ],
            [
                'roster_id'  => 3,
                'match_id'   => 3
            ],
            [
                'roster_id'  => 4,
                'match_id'   => 4
            ],
            [
                'roster_id'  => 5,
                'match_id'   => 5
            ],
        ];

        DB::table('match_opponents')->truncate();
        DB::table('match_opponents')->insert($matches);
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
