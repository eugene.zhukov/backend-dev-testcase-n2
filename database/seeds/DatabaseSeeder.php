<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RostersSeeder::class);
        $this->call(MatchesSeeder::class);
        $this->call(MatchOpponentsSeeder::class);
    }
}
